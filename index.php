<?php

mb_internal_encoding('UTF-8');

function mb_str_split($str, $length = 1)
{
	if ($length < 1) return FALSE;

	$result = array();

	for ($i = 0; $i < mb_strlen($str); $i += $length)
		$result[] = mb_substr($str, $i, $length);

	return $result;
}

$grid =
/*  0 */ 'KLOCKANAÄRB'.
/* 11 */ 'FEMTIOCDEFG'.
/* 22 */ 'KVARTJUGOHI'.
/* 33 */ 'MINUTERJIKL'.
/* 44 */ 'ÖVERMHALVNO'.
/* 55 */ 'ETTVÅTRELVA'.
/* 66 */ 'FYRAFEMSEXP'.
/* 77 */ 'SJUÅTTANIOQ'.
/* 88 */ 'TIOTOLVRSTU';

$words = array
	(
		'klockan' => array(0, 1, 2, 3, 4, 5, 6, 8, 9),

		'pfem'    => array(11, 12, 13),
		'ptio'    => array(14, 15, 16),
		'pkvart'  => array(22, 23, 24, 25, 26),
		'ptjugo'  => array(26, 27, 28, 29, 30),

		'minuter' => array(33, 34, 35, 36, 37, 38, 39),
		'i'       => array(41),
		'over'    => array(44, 45, 46, 47),
		'halv'    => array(49, 50, 51, 52),

		'ett'     => array(55, 56, 57),
		'tva'     => array(57, 58, 59),
		'tre'     => array(60, 61, 62),
		'fyra'    => array(66, 67, 68, 69),
		'fem'     => array(70, 71, 72),
		'sex'     => array(73, 74, 75),
		'sju'     => array(77, 78, 79),
		'atta'    => array(80, 81, 82),
		'nio'     => array(84, 85, 86),
		'tio'     => array(88, 89, 90),
		'elva'    => array(62, 63, 64, 65),
		'tolv'    => array(91, 92, 93, 94)
	);

list($seconds, $minutes, $hours) = localtime();

$lit = array();
$lit += $words['klockan'];

$minutes = round($minutes / 5) * 5;

switch ($minutes)
{
	case 0:
		break;
	case 5:
		$lit = array_merge($lit, $words['pfem']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['over']);
		break;
	case 10:
		$lit = array_merge($lit, $words['ptio']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['over']);
		break;
	case 15:
		$lit = array_merge($lit, $words['pkvart']);
		$lit = array_merge($lit, $words['over']);
		break;
	case 20:
		$lit = array_merge($lit, $words['ptjugo']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['over']);
		break;
	case 25:
		$lit = array_merge($lit, $words['pfem']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['i']);
		$lit = array_merge($lit, $words['halv']);
		break;
	case 30:
		$lit = array_merge($lit, $words['halv']);
		break;
	case 35:
		$lit = array_merge($lit, $words['pfem']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['over']);
		$lit = array_merge($lit, $words['halv']);
		break;
	case 40:
		$lit = array_merge($lit, $words['ptjugo']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['i']);
		break;
	case 45:
		$lit = array_merge($lit, $words['pkvart']);
		$lit = array_merge($lit, $words['i']);
		break;
	case 50;
		$lit = array_merge($lit, $words['ptio']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['i']);
		break;
	case 55:
		$lit = array_merge($lit, $words['pfem']);
		$lit = array_merge($lit, $words['minuter']);
		$lit = array_merge($lit, $words['i']);
		break;
}

/*

echo '<!-- ', PHP_EOL;
echo $hours, PHP_EOL;
echo $minutes, PHP_EOL;
echo $seconds, PHP_EOL;
echo '-->', PHP_EOL;

*/

if ($minutes > 20)
	$hours = ($hours + 1) % 12;
else
	$hours = $hours % 12;

switch ($hours)
{
	case 1: $lit = array_merge($lit, $words['ett']); break;
	case 2: $lit = array_merge($lit, $words['tva']); break;
	case 3: $lit = array_merge($lit, $words['tre']); break;
	case 4: $lit = array_merge($lit, $words['fyra']); break;
	case 5: $lit = array_merge($lit, $words['fem']); break;
	case 6: $lit = array_merge($lit, $words['sex']); break;
	case 7: $lit = array_merge($lit, $words['sju']); break;
	case 8: $lit = array_merge($lit, $words['atta']); break;
	case 9: $lit = array_merge($lit, $words['nio']); break;
	case 10: $lit = array_merge($lit, $words['tio']); break;
	case 11: $lit = array_merge($lit, $words['elva']); break;
	case 0: $lit = array_merge($lit, $words['tolv']); break;
}

header('Content-Type: text/html; charset=UTF-8');
header('Content-Language: sv-SE');
header('Refresh: 300');

?>
<!DOCTYPE html>
<html>
<head>
<title>Klocka</title>
<style type="text/css">
body
{
	background: #999;
}

#face
{
	background: black;
	margin: 2em 2em;
	padding: 4em;
	position: fixed;
	-moz-box-shadow: 1em 1em 2em black;
	-webkit-box-shadow: 1em 1em 2em black;
}

.character
{
	color: #222;
	display: inline-block;
	width: 1.5em;
	text-align: center;
	line-height: 1.75em;
	font-family: 'HelveticaCondensed', 'DIN Next LT Pro', 'Helvetica';
	font-size: 16pt;
}

.lit
{
	color: #bbb;
	text-shadow: 0 0 0.25em #bbb;
}
</style>
</head>
<body>
<div id="face">
<?php

$lines = mb_str_split($grid, 11);

foreach ($lines as $line_index => $line)
{
	foreach (mb_str_split($line) as $character_index => $character)
		echo '<div class="character', in_array($line_index * 11 + $character_index, $lit) ? ' lit' : '', '">', $character, '</div>';

	echo '<br />', PHP_EOL;
}

?>
</div>
</body>
</html>

